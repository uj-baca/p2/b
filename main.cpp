#include <stdio.h>
#include <stdlib.h>

int dir = 1;
int x = 0;
int y = 0;
int moved = 0;;

void doit(char *str, int n) {
    if (n == 0) {
        return;
    }
    moved = 1;
    char *it = str;
    while (*it) {
        char curr = *it++;
        if (curr == 'L') {
            dir += 1;
        }
        if (curr == 'B') {
            dir += 2;
        }
        if (curr == 'R') {
            dir += 3;
        }
        dir %= 4;
        if (dir == 0) {
            x += 1;
            printf(">");
        } else if (dir == 1) {
            y += 1;
            printf("^");
        } else if (dir == 2) {
            x -= 1;
            printf("<");
        } else if (dir == 3) {
            y -= 1;
            printf("v");
        }
        doit(str, n - 1);
    }
}

int main() {
    size_t count;
    int level;
    scanf("%d %d", &count, &level);
    char *data = (char *) malloc(count);
    scanf("%s", data);
    doit(data, level);
    if (moved) {
        printf("\n");
    }
    printf("%d %d", x, y);
    return 0;
}
